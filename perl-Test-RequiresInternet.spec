Name:           perl-Test-RequiresInternet
Version:        0.05
Release:        13
Summary:        Test network connection easily
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Test-RequiresInternet
Source0:        https://cpan.metacpan.org/authors/id/M/MA/MALLEN/Test-RequiresInternet-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) perl(Socket) perl(strict)
BuildRequires:  perl(warnings) perl(Test::More)

%description
Perl distribution Test-RequiresInternet, providing Perl modules: Test::RequiresInternet.
Easily test network connectivity.

%package help
Summary:        Help documents for perl-Test-RequiresInternet package

%description help
Help documents for perl-Test-RequiresInternet package.

%prep
%autosetup -n Test-RequiresInternet-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/Test/

%files help
%doc Changes README
%{_mandir}/man3/Test::RequiresInternet.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.05-13
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Feb 19 2020 wutao <wutao61@huawei.com> - 0.05-12
- Package init
